import {Routes} from "@angular/router";
import {MoviesComponent} from "../pages/movies/movies.component";
import {HomeComponent} from "../pages/home/home.component";
import {MovieDetailComponent} from "../pages/movie-detail/movie-detail.component";
/**
 * Created by yusuf on 2/2/17.
 */
export const routes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'movies', component: MoviesComponent },
    { path: 'movie', component: MovieDetailComponent }
];
