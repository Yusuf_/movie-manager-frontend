import { Component, OnInit } from '@angular/core';
import {MovieService} from "../../services/movie-service";
import {Movie} from "../../models/movie";
import {environment} from "../../../environments/environment";

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.css']
})
export class MovieDetailComponent implements OnInit {

  constructor(private movieService: MovieService) { }
  movie: Movie;

  ngOnInit() {
    this.getMovieDetails();


  }

  getMovieDetails(): void {
    this.movie = this.movieService.getMovieDetails()
  }
}
