import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { MoviesComponent } from './pages/movies/movies.component';
import { HeaderComponent } from './directives/header/header.component';
import {RouterModule} from "@angular/router";
import { HomeComponent } from './pages/home/home.component';
import {routes} from "./routes/app.routes";
import { CardComponent } from './directives/card/card.component';
import { MovieDetailComponent } from './pages/movie-detail/movie-detail.component';
import {MovieService} from "./services/movie-service";



@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    HeaderComponent,
    HomeComponent,
    CardComponent,
    MovieDetailComponent
  ],
  imports: [
    RouterModule.forRoot(routes),
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [MovieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
