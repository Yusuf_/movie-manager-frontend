import {CastMember} from "./cast_member";
/**
 * Created by yusuf on 2/2/17.
 */
export class Movie{
    public title: string;
    public runtime: number;
    public rating: number;
    public overview:string;
    public releaseDate: Date;
    public tagLine: string;
    public cast: CastMember[];
}
